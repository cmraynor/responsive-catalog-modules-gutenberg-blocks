<?php
/**
 * Plugin Name: Catalog Block Layouts
 * Plugin URI: https://bitbucket.org/cmraynor/responsive-catalog-modules-gutenberg-blocks/src/master/plugin.php
 * Description: A group of Gutenberg responsive catalog layout blocks.
 * Author: Claudette Raynor
 * Author URI: https://claudetteraynor.info
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
