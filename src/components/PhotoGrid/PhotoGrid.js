/**
 * BLOCK: catalog-layouts
 *
 * A series of catalog layout blocks for Gutenberg.
 */

//  Import CSS.
import './scss/editor.scss';
import './scss/style.scss';
import PhotoGridControls from './PhotoGridControls';
import parseString from '../../lib/helpers/parseString';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks; 


/**
 * Register: Catalog layouts Gutenberg
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'catalog-layouts/photo-grid', {
	title: __( 'Left Photo Grid' ), 
	icon: 'layout', 
	category: 'catalog-layouts-blocks', 
	supports: { align: [ 'wide', 'full'], default: 'full' },
	attributes: {
		selectedPostId: {
			type: 'string',
			default: ''
		}, 
		leftPhoto: {
			type: 'string',
			default: 'https://images.unsplash.com/photo-1505284361246-cedb2a8d2986?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1052&q=80'
		},
		rightPhotoOne: {
			type: 'string',
			default: 'https://images.unsplash.com/photo-1571139627661-cf707929f465?ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80'
		},
		rightPhotoTwo: {
			type: 'string',
			default: 'https://i.pinimg.com/236x/4f/03/8f/4f038fb00c862e60f1ab325ed61e9a71.jpg'
		},
		headline: {
			type: 'string',
			default: 'Headline'
		},
		subheadline: {
			type: 'string',
			default: 'SubHeadline'
		},
		excerpt: {
			type: 'string',
			default: 'Some descriptive text'
		},
		link: {
			type: 'string',
			default: '#'
		},
		linkText: {
			type: 'string',
			default: 'Learn More'
		}
	},
	keywords: [
		__( 'Catalog Layout — Photo Grid' ),
		__( 'Catalog Style Layout Block - Small Photo Grid' ),
		__( 'catalog-layout' ),
	],	
	/**
	 * The edit function describes the structure of the photo grid layout 
	 * block in the context of the editor.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		let leftPhoto = {
			backgroundImage: 'url(' + props.attributes.leftPhoto + ')'
		}
		let rightPhotoOne = {
			backgroundImage: 'url(' + props.attributes.rightPhotoOne + ')'
		}
		let rightPhotoTwo = {
			backgroundImage: 'url(' + props.attributes.rightPhotoTwo + ')'
		}
		return (
			<div className={ [ props.className, 'catalog-layout-block' ].join(' ') }>
				<div className="grid">
					<div style={leftPhoto}></div>
					<div>
						<div style={rightPhotoOne}></div>
						<div style={rightPhotoTwo}></div>
					</div>
			  	</div>
			 	<div className="content">
					<div>
						<h1>{parseString(props.attributes.headline)}</h1>
						<h2>{parseString(props.attributes.subheadline)}</h2>
						<p>{parseString(props.attributes.excerpt)}</p>
						<a href={encodeURI(props.attributes.link)} class="arrow-button"><span>&#8592;</span>{props.attributes.linkText}</a>
					</div>
				</div>
				<PhotoGridControls {...props}/>
			</div>	
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be
	 * combined into the final markup, which is then serialized by Gutenberg into
	 * post_content.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		let leftPhoto = {
			backgroundImage: 'url(' + props.attributes.leftPhoto + ')'
		}
		let rightPhotoOne = {
			backgroundImage: 'url(' + props.attributes.rightPhotoOne + ')'
		}
		let rightPhotoTwo = {
			backgroundImage: 'url(' + props.attributes.rightPhotoTwo + ')'
		}
		return (
			<div className={[ props.className, 'catalog-layout-block' ].join(' ') }>
				<div className="grid"> 
					<div style={leftPhoto}></div>
					<div>
						<div style={rightPhotoOne}></div>
						<div style={rightPhotoTwo}></div>
					</div>
			  	</div>
			 	<div className="content">
					<div>
						<h1>{parseString(props.attributes.headline)}</h1>
						<h2>{parseString(props.attributes.subheadline)}</h2>
						<p>{parseString(props.attributes.excerpt)}</p>
						<a href={encodeURI(props.attributes.link)} class="arrow-button"><span>&#8592;</span>{props.attributes.linkText} </a>
					</div>
				</div>
			</div>	
		);
	},
} );
