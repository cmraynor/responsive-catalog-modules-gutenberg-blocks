<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * Assets enqueued:
 * 1. blocks.style.build.css - Frontend + Backend.
 * 2. blocks.build.js - Backend.
 * 3. blocks.editor.build.css - Backend.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-block-editor} for WP editor styles and components.
 * @uses {wp-api-fetch} for API requests.
 * @since 1.0.0
 */
function catalog_layouts_block_assets() { // phpcs:ignore

	// Register block styles for both frontend + backend.
	wp_register_style(
		'catalog-layouts-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ),
		null 
	);

	// Register block editor script for backend.
	wp_register_script(
		'catalog-layouts-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ),  
		array( 'wp-blocks', 'wp-i18n', 'wp-api-fetch', 'wp-block-editor'), 
		null, 
		true
	);

	// Register block editor styles for backend.
	wp_register_style(
		'catalog-layouts-block-editor-css',
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ),
		array( 'wp-edit-blocks' ), 
		null
	);

	// WP Localized globals.
	wp_localize_script(
		'catalog-layouts-block-js',
		'cgbGlobal',
		[
			'pluginDirPath' => plugin_dir_path( __DIR__ ),
			'pluginDirUrl'  => plugin_dir_url( __DIR__ ),
		]
	);

	/**
	 * Register Gutenberg block on server-side.
	 *
	 * Register the block on server-side to ensure that the block
	 * scripts and styles for both frontend and backend are
	 * enqueued when the editor loads.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
	 * @since 1.16.0
	 */
	register_block_type(
		'catalog-layouts/photo-grid', array(
			// Enqueue blocks.style.build.css on both frontend & backend.
			'style'         => 'catalog-layouts-style-css',
			// Enqueue blocks.build.js in the editor only.
			'editor_script' => 'catalog-layouts-block-js',
			// Enqueue blocks.editor.build.css in the editor only.
			'editor_style'  => 'catalog-layouts-block-editor-css',
		)
	);
}

// Hook: Block assets.
add_action( 'init', 'catalog_layouts_block_assets' );

// Hook: Re-enqueueing blocks css in footer to reduce theme conflicts.
add_action('wp_enqueue_scripts', function() {
	wp_dequeue_style('catalog-layouts-style-css');
	wp_enqueue_style('catalog-layouts-style-css');		
}, 40 );

/**
 * Registering custom category for catalog layouts blocks.
 *
 * @since 1.0.0
 */
function catalog_layouts_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'catalog-layouts-blocks',
				'title' => __( 'Catalog Layout Blocks', 'catalog-layouts-blocks' ),
			),
		)
	);
}
add_filter( 'block_categories', 'catalog_layouts_block_category', 10, 2 );